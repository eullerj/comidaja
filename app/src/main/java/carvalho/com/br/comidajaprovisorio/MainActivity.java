package carvalho.com.br.comidajaprovisorio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void callSobre(View view){
//        Intent intent = new Intent (this, Sobre.class);
//        startActivity(intent);
        startActivity(new Intent(this, Sobre.class));
    }

    public void callCardapio(View view){
//        Intent intent = (this, )
        startActivity(new Intent(this, Cardapio.class));
    }
}
